#!/usr/bin/env python

# this is the scipy stack, e.g., MATLAB 4 no $
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

# this is for interacting with NIFTI files
import nibabel as nib

# files of interest
data = 'func.nii.gz'
mask = 'mask.nii.gz'
rois = 'rois.nii.gz'
roi  = 1

# nibabel has a look into the data
data = nib.load(data)
affine = data.get_affine()
header = data.get_header()

# what are the dimensions of our data?
dims = data.shape

# get the actual file
data = data.get_data()

# reshape to voxels * timepoints (4D --> 2D)
data = data.reshape(dims[0]*dims[1]*dims[2], dims[3])

# now do the same thing for rois
rois = nib.load(rois).get_data()
rois = rois.reshape(dims[0]*dims[1]*dims[2], 1)

# get the seed time series
idx = np.where(rois == roi)[0]
ts = np.mean(data[idx, :], axis=0)

# look at the timeseries
plt.plot(ts)
plt.show()
len(ts) # looks like it's the right length (i.e., our axis is correct)

# make an output matrix
output = np.zeros(dims[0]*dims[1]*dims[2])

# correlate seed against all voxels
for i in range(dims[0]*dims[1]*dims[2]):
    output[i] = np.corrcoef(ts, data[i, :])[0][1]

# get back to 4D
output = np.reshape(output, (dims[0], dims[1], dims[2], 1))

# write the results into a NIFTI file
output = nib.nifti1.Nifti1Image(output, affine)
output.to_filename('seed-based-correlation_{}.nii.gz'.format(roi))


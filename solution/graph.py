#!/usr/bin/env python

# this is the scipy stack, e.g., MATLAB 4 no $
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

# this is for interacting with NIFTI files
import nibabel as nib

# load data
data = 'func.nii.gz'
rois = 'rois.nii.gz'
data = nib.load(data)
dims = data.shape
data = data.get_data()
rois = nib.load(rois).get_data()
data = data.reshape(dims[0]*dims[1]*dims[2], dims[3])
rois = rois.reshape(dims[0]*dims[1]*dims[2], 1)

# create output matrix
output = np.zeros((len(np.unique(rois))-1, dims[3]))

# load in all mean time series from atlas
for i, roi in enumerate(np.unique(rois)):
    if roi > 0:
        idx = np.where(rois == roi)[0]
        ts = np.mean(data[idx, :], axis=0)
        output[i-1, :] = ts

# correlation matrix
G = np.corrcoef(output)

# view output
plt.imshow(G, interpolation='nearest', cmap=plt.cm.RdBu_r)
plt.colorbar()

